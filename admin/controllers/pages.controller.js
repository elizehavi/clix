'use strict';

const db = require('./../../lib/db');

function getPages(siteId) {
    return db.getPages(siteId).toArray();
}

module.exports = {
    getPages: getPages
};
