'use strict';

const db = require('./../../lib/db');

function getStats(query) {
    const overallStatsPromise = db.getOverallStats(query);
    const topPagesPromise = db.getTopPages(query);
    const statsByDay = db.getStatsByDay(query);

    return Promise.all([overallStatsPromise, topPagesPromise, statsByDay])
        .then((values) => {
            const result = values[0];
            result.topPages = values[1];
            result.ranges = values[2];

            return result;
        });
}

module.exports = {
    getStats: getStats
};
