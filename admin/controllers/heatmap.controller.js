'use strict';

const db = require('./../../lib/db');

function getHeatmapStats(query) {
    return db.getHeatmapStats(query);
}

module.exports = {
    getHeatmapStats: getHeatmapStats
};
