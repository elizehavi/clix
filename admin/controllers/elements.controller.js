'use strict';

const db = require('./../../lib/db');

function getElementStats(reqQuery){
    return db.getElementStats(reqQuery)
        .then((result) => {
            const element = result.element;
            const totalElementsClicks = result.totalElementsClicks;
            const hoverConversions = (element.clicks / element.hovers) * 100;
            const averageOrder = Math.floor(element.totalOrder / element.clicks);
            const popularity = (element.clicks / totalElementsClicks) * 100;

            const elemStats = {
                clicks: element.clicks,
                popularity: popularity,
                hoverConversions: hoverConversions,
                averageOrder: averageOrder
            };

            return elemStats;
        });
}

module.exports = {
    getElementStats : getElementStats
};
