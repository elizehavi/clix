'use strict';

const db = require('./../../lib/db');

function getCategoryFunnel(siteId, categoryId, from, to) {
    return db.getCategoryFunnel(siteId, categoryId, from, to);
}

module.exports = {
    getCategoryFunnel: getCategoryFunnel
};
