'use strict';

const express = require('express');
const router = express.Router();
const helpers = require('./../../lib/helpers');
const controller = require('./../controllers/elements.controller');

router.get('/stats', (req, res) =>{

    const query = req.query;
    const siteId = query.siteId;
    const pageId = query.pageId;
    const elementId = query.elementId;
    const from = helpers.getDate(query.from);
    const to = helpers.getDate(query.to);
    let toPlusOne = to.getTime() + (60000 * 60 * 24);
    toPlusOne = new Date(toPlusOne);

    if (!query.siteId || !query.pageId ||
      !query.elementId || !query.from || !query.to) {
        return res.status(404).send('request "/elements/stats" missing query parameters');
    }

    const request = {
        siteId: siteId,
        pageId: pageId,
        elementId: elementId,
        from: from,
        to: toPlusOne
    };

    controller.getElementStats(request)
        .then((dbRes) =>{
            res.send(dbRes);
        })
        .catch((err) =>{
            helpers.handleError(res, 'elements', err);
        });
});

module.exports = router;
