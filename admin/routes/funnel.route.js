'use strict';

const express = require('express');
const router = express.Router();
const helpers = require('./../../lib/helpers');
const controller = require('./../controllers/funnel.controller');

router.get('/', (req, res) => {
    const q = req.query;
    if (!q || !q.siteId || !q.categoryId || !q.from || !q.to) {
        return res.status(400)
            .send('request get/funnel missing query parameters (siteId, category, dateFrom, dateTo)');
    }

    const siteId = q.siteId;
    const categoryId = q.categoryId;
    const from = new Date(q.from);
    const to = new Date(q.to);

    from.setUTCHours(0,0,0,0);
    to.setUTCHours(0,0,0,0);

    controller.getCategoryFunnel(siteId, categoryId, from, to)
        .then((dbRes) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(dbRes);
        })
        .catch((err, res) => helpers.handleError(res, 'funnel', err));
});

module.exports = router;
