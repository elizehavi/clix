'use strict';

const funnel = require('./funnel.route');
const elements = require('./elements.route');
const dashboard = require('./dashboard.route');
const heatmap = require('./heatmap.route');
const pages = require('./pages.route');

function setup(app) {
    app.use('/funnel', funnel);
    app.use('/elements', elements);
    app.use('/dashboard', dashboard);
    app.use('/pages', pages);
    app.use('/heatmap', heatmap);
}

module.exports = setup;
