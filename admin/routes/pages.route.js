'use strict';

const express = require('express');
const router = express.Router();
const controller = require('./../controllers/pages.controller');
const helpers = require('./../../lib/helpers');

router.get('/:siteId', (req, res) => {
    controller.getPages(req.params.siteId)
        .then((resp) => {
            return res.send(resp);
        })
        .catch((err) => helpers.handleError(res, '/admin/pages', err));
});

module.exports = router;
