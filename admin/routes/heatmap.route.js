'use strict';

const helpers = require('./../../lib/helpers.js');
const express = require('express');
const router = express.Router();
const controller = require('./../controllers/heatmap.controller');
const types = {
    clicks: 'clickCoords',
    hovers: 'hoverCoords'
};

router.get('/:type', (req, res) => {
    if (!types[req.params.type]) {
        return res.status(404).send('Invalid type');
    }

    const query = {
        siteId: req.query.siteId,
        pageId: req.query.pageId,
        from: helpers.getDate(req.query.from),
        to: helpers.getDate(req.query.to),
        type: types[req.params.type]
    };

    controller
        .getHeatmapStats(query)
        .then((result) => res.send(result))
        .catch((err) => helpers.handleError(res, 'dashboard', err));
});

module.exports = router;

