'use strict';

const helpers = require('./../../lib/helpers.js');
const express = require('express');
const router = express.Router();
const controller = require('./../controllers/dashboard.controller');

router.get('/', (req, res) => {
    const query = {
        siteId: req.query.siteId,
        from: helpers.getDate(req.query.from),
        to: helpers.getDate(req.query.to)
    };

    controller
        .getStats(query)
        .then((result) => res.send(result))
        .catch((err) => helpers.handleError(res, 'dashboard', err));
});

module.exports = router;
