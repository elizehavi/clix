'use strict';

angular.module('funnel',['ngMaterial'])
    .controller('mainCtrl', ['$scope','clix',function($scope, clix){
        var demoSiteId = '1';

        init();

        function init() {
            $scope.errorOccurred = false;
            $scope.noData = false;

            // give default values, for testing
            $scope.categoryId = "shoes";
            $scope.from = "2016-09-02";
            $scope.to = "2016-09-18";
        }

        $scope.onAnalyseButtonClick = function() {
            $scope.notification = null;
            $scope.funnelSteps = null;
            $scope.funnel = null;

            if (!$scope.categoryId || !$scope.from || !$scope.to) {
                $scope.notification = "Please make sure all of the parameters are filled.\n" +
                    "(Category, From, To)";
                return console.log($scope.notification);
            }

            clix.getFunnel(demoSiteId, $scope.categoryId, $scope.from, $scope.to)
                .then((response) => {
                    var funnel = response.data;
                    console.log('funnel:', funnel);

                    $scope.funnel = funnel;

                    var categoryName = $scope.categoryId;

                    if (!funnel.homepage || !funnel.category) {
                        return $scope.notification = funnel;

                    }

                    $scope.funnelSteps = [
                        {
                            domId: 'homepage',
                            title: 'Homepage',
                            visitors: funnel.homepage.visitors,
                            left: funnel.homepage.leftPercent,
                            continued: funnel.homepage.enteredNext
                        },
                        {
                            domId: 'category',
                            title: categoryName,
                            visitors: funnel.category.visitors,
                            left: funnel.category.leftPercent,
                            continued: funnel.category.enteredNext
                        },
                        {
                            domId: 'purchases',
                            title: 'Puchases',
                            visitors: funnel.category.purchases,
                        }
                    ];

                })
                .catch((err) => {
                    $scope.notification = "Something went wrong.\n" +
                        "But hey, you still got your looks."
                    console.log('Error: GET/funnel',err);
                });
        }
    }])
    .service('clix', ['$http', function($http){
        var host = {
            collector: '',
            admin: ''
        };

        function getPages() {
            return $http({
                method: 'GET',
                url: host.admin + "/pages/1"
            });
        }

        function getFunnel(siteId, categoryId, from, to) {
            return $http.get(host.admin + `/funnel?siteId=${siteId}&categoryId=${categoryId}&from=${from}&to=${to}`);
        }

        return {
            getPages: getPages,
            getFunnel: getFunnel
        }
    }]);
