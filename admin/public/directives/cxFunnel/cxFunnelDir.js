angular.module('funnel')
.directive('cxStep', [function() {
    return {
        templateUrl: "directives/cxFunnel/cxFunnelStep.html",
        scope: {
            domId: "=",
            title: "=",
            visitors: "=",
            left: "=",
            continued: "="
        }
    }
}]);