'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./../lib/logger');
const db = require('./../lib/db');
const routes = require('./routes');
const app = express();

/*
 * Cross Middlewares and static files
 */
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

/*
 * Routes
 */
routes(app);

/*
 * Exception Handling
 */
app.use((err, req, res, next) => {
    logger.error(err.stack);

    return res.status(500).send('Unexpected ERror');
});

/*
 * DB Setup and Initialization
 */
db.setup()
    .then(() => {
        app.listen(8081, function() {
            logger.info('admin is listening on port 8081!');
        });
    })
    .catch((err) => {
        logger.error(`Please verify mongodb is online: ${err}`);
        process.exit(1);
    });
