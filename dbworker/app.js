'use strict';

const db = require('./../lib/db.js');
const logger = require('./../lib/logger');
const CronJob = require('cron').CronJob;

let job;

db.setup()
    .then(() => {
        job = new CronJob({
            cronTime: '0 0 23 * * *',
            onTick: () => {
                db.createEmptyDailyStatsForNextDay()
                    .then(() => logger.info('empty daily status for tommorow inserted'))
                    .catch((err) => logger.error(`empty daily stats error: ${err}`));
            },
            start: true
        });
    })
    .catch((err) => {
        logger.error(`Please verify mongodb is online: ${err}`);
        process.exit(1);
    });
