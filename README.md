# Welcome to Clix repository! #

There are 6 "sub" node projects in this repo:

1. [Collector](#collector)
2. [Admin](#admin)
3. [DBWorker](#dbworker)
4. [Lib](#lib)
5. [Deploy](#deploy)

### Install
To install the project:
```
npm install
```

Then, to import a sample database data into your local MongoDB instance, run (this will delete old collections in clixdb):
```
npm run import
```

Then, to run all projects at once with forever (need to install it globally
first):
```
npm start
```

#### Listening
- Collector will listen on port 8080
- Admin will listen on port 8081

If you want to start a specific application and without forever, see
instructions below per application.

### Postman Requests
Inside the repo, there is a clix.postman_collection file which you can import
to postman to send all kinds of requests to our applications.

Please note all queries are against the running applications on the mta cloud.

### Collector
The collector application is the API for the clients that send events while
users are browsing the publisher site.

To run the collector application:
```
npm run collector
```

### Admin
The admin application is the API for the administrators that want to view
statistics and data about their users.

To run the admin application:
```
npm run admin
```

### DBWorker
The dbworker runs a periodic task once a day to insert empty stats docs for the day after tommorow (preparation for collector insertions).

To run the dbworker application:
```
npm run dbworker
```

### Lib
The lib directory contains some common modules for use in all other projects:

* Logger
* DB
* Helpers
* Config

### Deploy
The deploy directory contains some scripts for:

* Publish the system to the mta cloud (remove old version, upload and start running)
* Generate random data for the database
* Import data to local MongoDB instance
* Start all applications at once with forever

### Any Questions?

* Eli Zehavi <eli.zehavi@gmail.com>
* Hagai Nuriel <hagai.nuriel@gmail.com>
* Yakir Rabinovich <yagiro@gmail.com>