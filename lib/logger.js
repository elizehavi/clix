'use strict';

const logger = require('winston');

logger.infoJSON = (obj) => {
    logger.info(JSON.stringify(obj, null, 4));
};

module.exports = logger;