'use strict';

const config = require('./../config.json').mongo;
const logger = require('./logger');
const helpers = require('./helpers');

const intervalMs = 60000;
let bulk;
let db;
let isInitialized = false;

const eventToFieldMap = {
    view: 'views',
    visit: 'visitors',
    purchase: 'purchases'
};

function setup(dbInstance) {
    db = dbInstance;

    if (!isInitialized) {
        bulk = db.collection(config.collections.PagesDailyStats).initializeUnorderedBulkOp();
        setInterval(sendBulk, intervalMs);
    }

    isInitialized = true;
}

function updatePage(eventType, siteId, pageId) {
    const fieldToInc = eventToFieldMap[eventType];
    const findQuery = {
        siteId: siteId,
        pageId: pageId,
        date: helpers.getToday()
    };
    const updateQuery = {
        $inc: {
            [fieldToInc]: 1
        }
    };

    let bulkRes = bulk
        .find(findQuery)
        .update(updateQuery);

    return Promise.resolve(bulkRes);
}

function sendBulk() {
    if (bulkHasOperations(bulk)) {
        bulk.execute();
        logger.info('bulk saved events (view, visit, purchase).');
        bulk = db
            .collection(config.collections.PagesDailyStats)
            .initializeUnorderedBulkOp();
    }
}

function bulkHasOperations(b) {
    return b && b.s && b.s.currentBatch && b.s.currentBatch.operations && b.s.currentBatch.operations.length > 0;
}

module.exports = {
    setup: setup,
    updatePage: updatePage
};
