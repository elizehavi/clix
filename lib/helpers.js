'use strict';

const logger = require('./logger');

function getToday() {
    const d = new Date();

    d.setUTCHours(0, 0, 0, 0);

    return d;
}

function getDate(dateString) {
    let splitDate = dateString.split('-');
    let date = new Date(splitDate[2], Number(splitDate[1]) - 1, splitDate[0]);

    date.setUTCHours(0, 0, 0, 0);

    return date;
}

function getTomorrow() {
    let d = (new Date()).setUTCHours(0, 0, 0, 0);
    d += 60000 * 60 * 24;
    return new Date(d);
}

function handleError(res, route, err) {
    logger.error(`error in ${route} route: ${JSON.stringify(err, null, 4)}`);
    if (err.message) {
        return res.status(500).send(err.message);
    }
    return res.status(500).send('Unexpected error occured');
}

module.exports = {
    getToday: getToday,
    getDate: getDate,
    getTomorrow: getTomorrow,
    handleError: handleError
};
