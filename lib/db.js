'use strict';

const bluebird = require('bluebird');
const mongoClient = require('mongodb').MongoClient;
const logger = require('./logger');
const helpers = require('./helpers');
const config = require('./../config.json').mongo;
const pages = require('./db-pages.js');

const mongoUrl = config.url;
let db;

bluebird.promisifyAll(mongoClient);

function setup() {
    return mongoClient
        .connect(mongoUrl)
        .then((dbInstance) => {
            logger.info('connected to mongodb server');
            db = dbInstance;
            pages.setup(db);

            bluebird.promisifyAll(db);
        });
}

function updateHover(siteId, hoverDoc) {
    const collection = db.collection(config.collections.ElementsDailyStats);
    const findQuery = buildFindQuery(siteId, hoverDoc);
    const updateQuery = {
        $inc: {
            hovers: hoverDoc.hoverCount,
            totalTime: hoverDoc.totalTime
        }
    };

    return collection
        .updateOne(findQuery, updateQuery)
        .then(logResult);
}

function updatePageClicks() {
    return Promise.resolve();
}

function updateClick(siteId, clickDoc) {
    const collection = db.collection(config.collections.ElementsDailyStats);
    const findQuery = buildFindQuery(siteId, clickDoc);
    const updateQuery = {
        $inc: {
            clicks: clickDoc.clickCount,
            totalOrder: clickDoc.totalOrder
        }
    };

    return collection
        .updateOne(findQuery, updateQuery)
        .then(logResult);
}

function buildFindQuery(siteId, doc) {
    return {
        elementId: doc.elementId,
        pageId: doc.pageId,
        siteId: siteId,
        date: helpers.getToday()
    };
}

function getUnorderedBulk(collectionName) {
    return db.collection(collectionName).initializeUnorderedBulkOp();
}

function updateHeatMap(heatmapEvent, siteId, statsType){
    const statsField = statsTypeToArrayName[statsType] + '.$.value';
    const collection = db.collection(config.collections.HeatMapDailyStats);
    const statsTypeToArrayName = {
        clicks: 'clickCoords',
        hovers: 'hoverCoords'
    };
    const findExistingQuery = {
        siteId: siteId,
        pageId: heatmapEvent.pageId,
        date: helpers.getToday(),
        [statsTypeToArrayName[statsType]]: {
            $elemMatch: {x: heatmapEvent.x, y: heatmapEvent.y}
        }
    };
    const updateExistingQuery = {
        $inc: { [statsField]: heatmapEvent.value }
    };
    const findNonExistingQuery = {
        siteId: siteId,
        pageId: heatmapEvent.pageId,
        date: helpers.getToday()
    };
    const updateNonExistingQuery = {
        $push: {
            [statsTypeToArrayName[statsType]]: { x: heatmapEvent.x, y: heatmapEvent.y, value: heatmapEvent.value }
        }
    };

    return collection.updateOne(findExistingQuery, updateExistingQuery)
        .then((result) => {
            if (result.result.nModified === 0) {
                return collection
                    .updateOne(findNonExistingQuery, updateNonExistingQuery)
                    .then(logResult);
            }
        });
}

function getCategoryFunnel(siteId, categoryId, from, to) {
    const collection = db
        .collection(config.collections.PagesDailyStats);

    return collection.aggregate([
        {
            $match: {
                siteId: siteId,
                $or: [
                    { pageId: categoryId },
                    { pageId: 'homepage'}
                ],
                date: {
                    $gte: from,
                    $lte: to
                }
            }
        },
        {
            $group: {
                _id: {
                    siteId: '$siteId',
                    pageId: '$pageId'
                },
                visitors: { $sum: '$visitors' },
                purchases: { $sum: '$purchases' }
            }
        }
    ]).toArray()
      .then((pagesStats) => {
          let homepageData = null, categoryData = null;

          pagesStats.forEach((page) => {
              if (page['_id']['pageId'] === categoryId) {
                  categoryData = {
                      visitors: page.visitors,
                      purchases: page.purchases
                  };
              }
              else {
                  homepageData = {
                      visitors: page.visitors
                  };
              }
          });

          // No problem here, just a default empty value
          let funnel = 'No data for given dates.';

          if (!categoryData) {
              funnel = `Missing data for category \"${categoryId}\" in the given dates.`;
          }
          else if (!homepageData) {
              funnel = 'Missing data for homepage in the given dates.';
          }

          if (pagesStats.length > 0 && categoryData && homepageData) {
              funnel = {
                  homepage: {
                      visitors: homepageData.visitors,
                      leftPercent: ((homepageData.visitors - categoryData.visitors) / homepageData.visitors) * 100,
                      enteredNext: (categoryData.visitors / homepageData.visitors) * 100
                  },
                  category: {
                      visitors: categoryData.visitors,
                      leftPercent: ((categoryData.visitors - categoryData.purchases) / categoryData.visitors) * 100,
                      enteredNext: (categoryData.purchases / categoryData.visitors) * 100,
                      purchases: categoryData.purchases
                  }
              };
          }

          return funnel;
      });
}

function getOverallStats(query) {
    const collection = db.collection(config.collections.PagesDailyStats);
    const pipeline = [
        {
            $match: {
                siteId: query.siteId,
                date: {
                    $gte: query.from,
                    $lte: query.to
                }
            }
        },
        {
            $group: {
                _id: '$siteId',
                views: { $sum: '$views' },
                visitors: { $sum: '$visitors' },
                purchases: { $sum: '$purchases' }
            }
        }
    ];

    return collection
      .aggregate(pipeline)
      .toArray()
      .then((result) => {
          if (!result || result.length === 0) {
              return {
                  views: 0,
                  visitors: 0,
                  purchases: 0
              };
          }

          return result[0];
      });
}

function getTopPages(query) {
    const collection = db.collection(config.collections.PagesDailyStats);
    const pipeline = [
        {
            $match: {
                siteId: query.siteId,
                date: {
                    $gte: query.from,
                    $lte: query.to
                }
            }
        },
        {
            $group: {
                _id: '$pageId',
                views: { $sum: '$views' }
            }
        },
        { $sort: { views: -1 } }
    ];

    return collection
        .aggregate(pipeline)
        .toArray();
}

function getStatsByDay(query) {
    const collection = db.collection(config.collections.PagesDailyStats);
    const pipeline = [
        {
            $match: {
                siteId: query.siteId,
                date: {
                    $gte: query.from,
                    $lte: query.to
                }
            }
        },
        {
            $group: {
                _id: '$date',
                visitors: { $sum: '$visitors' },
                purchases: { $sum: '$purchases' }
            }
        },
        { $sort: { _id: 1 } }
    ];

    return collection
        .aggregate(pipeline)
        .toArray();
}

function getHeatmapStats(query) {
    const array = query.type;
    const collection = db.collection(config.collections.HeatMapDailyStats);
    const pipeline = [
        {
            $match: {
                siteId: query.siteId,
                pageId: query.pageId,
                date: {
                    $gte: query.from,
                    $lte: query.to
                }
            }
        },
        {
            $unwind: '$' + array
        },
        {
            $group: {
                _id: {
                    x: `$${array}.x`,
                    y: `$${array}.y`
                },
                value: { $sum: `$${array}.value` }
            }
        },
        {
            $project: {
                _id: false,
                x: '$_id.x',
                y: '$_id.y',
                value: '$value'
            }
        }
    ];

    return collection
        .aggregate(pipeline)
        .toArray();
}

function getTotalClicksInPage(siteId, pageId, from, to) {
    const collection = db.collection(config.collections.PagesDailyStats);
    const matchQuery = {
        $match: {
            siteId: siteId,
            pageId: pageId,
            date: {
                $gte: from,
                $lte: to
            }
        }
    };
    const groupQuery = {
        $group: {
            _id: 'pageId',
            totalElementsClicks: { $sum : '$totalElementsClicks' }
        }
    };
    const pipeline = [ matchQuery, groupQuery ];

    return collection
        .aggregate(pipeline)
        .toArray()
        .then((res) => {
            if (!res[0] || !res[0].totalElementsClicks) {
                return;
            }
            return (res[0]).totalElementsClicks;
        });
}

function getElementStats(reqQuery) {
    const collection = db.collection(config.collections.ElementsDailyStats);
    const matchQuery = {
        $match: {
            siteId: reqQuery.siteId,
            pageId: reqQuery.pageId,
            elementId: reqQuery.elementId,
            date: {
                $gte: reqQuery.from,
                $lte: reqQuery.to
            }
        }
    };
    const groupQuery = {
        $group: {
            _id: '$elementId',
            clicks: { $sum: '$clicks' },
            hovers: { $sum: '$hovers' },
            totalTime: { $sum: '$totalTime' },
            totalOrder: { $sum: '$totalOrder' }
        }
    };

    let pipeLine = [matchQuery, groupQuery];
    let p1 = collection.aggregate(pipeLine).toArray();
    let p2 = getTotalClicksInPage(reqQuery.siteId, reqQuery.pageId, reqQuery.from, reqQuery.to);

    return Promise.all([p1, p2])
        .then((values) => {
            if (!values[0] || values[0].length == 0) {
                throw new Error('Aggregation query returned an empty result');
            }

            const returnedStats = {
                element: values[0][0],
                totalElementsClicks: values[1]
            };

            return returnedStats;
        })
        .catch(() => {
            throw new Error('Aggregation query returned an empty result');
        });
}

function createEmptyDailyStatsForNextDay() {
    const pagesCollection = db.collection(config.collections.Pages);
    const dailyPagePrototype = {
        pageId: '',
        siteId: '',
        date: helpers.getTomorrow(),
        totalElementsClicks: 0,
        views: 0,
        visitors: 0,
        purchases: 0
    };
    const dailyElementPrototype = {
        elementId: '',
        pageId: '',
        siteId: '',
        date: helpers.getTomorrow(),
        clicks: 0,
        hovers: 0,
        totalTime: 0,
        totalOrder: 0
    };
    const dailyHeatmapPrototype = {
        pageId: '',
        siteId: '',
        date: helpers.getTomorrow(),
        clickCoords: [],
        hoverCoords: []
    };

    const tasks = [];

    pagesCollection.find().forEach((page) => {
        tasks.push(createEmptyPageDailyStats(page, dailyPagePrototype));
        tasks.push(createEmptyPageDailyHeatmap(page, dailyHeatmapPrototype));

        if (page.elements) {
            page.elements.forEach((element) => {
                tasks.push(createEmptyElementDailyStats(page, element, dailyElementPrototype));
            });
        }
    });

    return Promise.all(tasks);
}

function createEmptyPageDailyStats(page, dailyPagePrototype) {
    delete dailyPagePrototype['_id'];
    dailyPagePrototype.siteId = page.siteId;
    dailyPagePrototype.pageId = page.pageId;

    return db.collection(config.collections.PagesDailyStats).insert(dailyPagePrototype);
}

function createEmptyPageDailyHeatmap(page, dailyHeatmapPrototype) {
    delete dailyHeatmapPrototype['_id'];
    dailyHeatmapPrototype.siteId = page.siteId;
    dailyHeatmapPrototype.pageId = page.pageId;

    return db.collection(config.collections.HeatMapDailyStats).insert(dailyHeatmapPrototype);
}

function createEmptyElementDailyStats(page, element, dailyElementPrototype) {
    delete dailyElementPrototype['_id'];
    dailyElementPrototype.siteId = page.siteId;
    dailyElementPrototype.pageId = page.pageId;
    dailyElementPrototype.elementId = element.id;

    return db.collection(config.collections.ElementsDailyStats).insert(dailyElementPrototype);
}

function logResult(result) {
    logger.debug(`result from mongo: ${JSON.stringify(result, null, 4)}`);
}

function getDb() {
    return db;
}

function getPages(siteId) {
    return db.collection(config.collections.Pages).find({ siteId: siteId });
}

module.exports = {
    setup: setup,
    updateClick: updateClick,
    updateHover: updateHover,
    updateHeatMap: updateHeatMap,
    updatePageClicks: updatePageClicks,
    getUnorderedBulk: getUnorderedBulk,
    updatePage: pages.updatePage,
    getPages: getPages,
    getCategoryFunnel: getCategoryFunnel,
    getOverallStats: getOverallStats,
    getTopPages: getTopPages,
    getStatsByDay: getStatsByDay,
    getElementStats: getElementStats,
    getHeatmapStats: getHeatmapStats,
    createEmptyDailyStatsForNextDay: createEmptyDailyStatsForNextDay,
    getDb: getDb
};
