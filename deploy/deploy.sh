rm -rf deploy/clix
mkdir deploy/clix
rsync -a --exclude='.git' --exclude='node_modules' --exclude='deploy/clix' . deploy/clix
cd deploy
tar -zcvf clix.tar.gz clix
cd ..
rm -rf deploy/clix
node deploy/deploy.js $1 "forever stopall && rm -rf clix && mkdir clix"
scp -v deploy/clix.tar.gz webstud@mobedu6.mtacloud.co.il:clix.tar.gz
rm -rf deploy/clix.tar.gz
node deploy/deploy.js $1 "tar -zxvf clix.tar.gz && cd clix && npm install && cd admin/public && npm install && cd .. && cd .. && npm run import && forever stopall && forever start collector/app.js && forever start admin/app.js && forever start dbworker/app.js && forever list"
echo "done"
