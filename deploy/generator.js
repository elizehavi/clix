'use strict';

const fs = require('fs');
const logger = require('./../lib/logger');

const clicksMin = 0;
const clicksMax = 150;
const hoversMin = 150;
const hoversMax = 400;
const orderMin = 1;
const orderMax = 5;
const viewsMin = 1000;
const viewsMax = 5000;
const visitorsMin = 500;
const visitorsMax = 1000;
const purchasesMin = 200;
const purchasesMax = 400;

const schemaArchitecture = {
    siteId: '1',
    pageIds: ['shoes','sport','food', 'homepage'],
    elemIds: ['e1','e2','e3','e4','e5'],
    from: new Date(2016,8,2,0,0,0,0),
    to: new Date(2016,8,19,0,0,0,0),
    numOfClicksPerPage: 10
};

let generatedSchema = {
    Users: [],
    Pages: [],
    ElementsDailyStats: [],
    PagesDailyStats: [],
    HeatMapDailyStats: []
};

function getRandomInt(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}

function generateDataBase() {
    generatedSchema.Users.push({
        email: 'abc@gmail.com',
        passwordHash: '123456',
        siteId: '1',
        siteName: 'Ahalan'
    });

    generateStaticPages();

    for (let dateCount = new Date(schemaArchitecture.from);
         dateCount <= new Date(schemaArchitecture.to);
         dateCount.setDate(dateCount.getDate() + 1)) {
        generateDailyElemStats(dateCount);
        generateDailyPagesStats(dateCount);
        generateDailyHeatMapStats(dateCount);
    }
}

function generateStaticPages() {
    for (let i = 0; i < schemaArchitecture.pageIds.length; ++i) {
        let page = {
            siteId: schemaArchitecture.siteId,
            pageId: schemaArchitecture.pageIds[i],
            name: 'somePage' + i,
            type: 'page',
            elements: []
        };

        for (let j = 0; j < schemaArchitecture.elemIds.length; j++) {
            let elem = {
                id: schemaArchitecture.elemIds[j],
                name: 'someElem'
            };

            page.elements.push(elem);
        }

        generatedSchema.Pages.push(page);
    }
}

function generateDailyElemStats(date) {
    for (let i = 0; i < schemaArchitecture.pageIds.length; ++i) {
        for (let j = 0; j <schemaArchitecture.elemIds.length; ++j) {
            let elem = {
                siteId: schemaArchitecture.siteId,
                pageId: schemaArchitecture.pageIds[i],
                elementId:schemaArchitecture.elemIds[j],
                date: new Date(date.getTime()),
                clicks: getRandomInt(clicksMin, clicksMax),
                hovers: getRandomInt(hoversMin, hoversMax),
                totalTime: getRandomInt(5,10),
                totalOrder:getRandomInt(orderMin, orderMax)
            };

            generatedSchema.ElementsDailyStats.push(elem);
        }
    }
}

function generateDailyPagesStats(date){
    for (let i = 0; i < schemaArchitecture.pageIds.length; ++i) {

        let totalClicks = getTotalClicksForPage(schemaArchitecture.pageIds[i]);
        let page = {
            siteId:schemaArchitecture.siteId,
            pageId:schemaArchitecture.pageIds[i],
            date:new Date(date.getTime()),
            totalElementsClicks: totalClicks,
            views: getRandomInt(viewsMin, viewsMax),
            visitors: getRandomInt(visitorsMin, visitorsMax),
            purchases: getRandomInt(purchasesMin, purchasesMax)
        };

        if (page.pageId == 'homepage') {
            page.visitors += visitorsMax;
            page.purchases = 0;
        }

        generatedSchema.PagesDailyStats.push(page);
    }
}

function generateDailyHeatMapStats(date){
    date.setUTCHours(0, 0, 0, 0);

    for (let i = 0; i < schemaArchitecture.pageIds.length; ++i) {
        let clickCoords = getRandomCoords([]);
        let hoverCoords = clickCoords.slice(0,clickCoords.length);
        let dailyHeatMap = {
            siteId: schemaArchitecture.siteId,
            pageId: schemaArchitecture.pageIds[i],
            date: new Date(date.getTime()),
            clickCoords: clickCoords,
            hoverCoords: getRandomCoords(hoverCoords)
        };

        generatedSchema.HeatMapDailyStats.push(dailyHeatMap);
    }
}

function getRandomCoords(coords) {
    let numOfCoords = getRandomInt(10, 30);

    for (let i = 0; i < numOfCoords; i++) {
        let coord = {
            x: getRandomInt(0,100),
            y: getRandomInt(0,100),
            value: getRandomInt(1,10)
        };

        if (!coordExists(coords, coord)) {
            coords.push(coord);
        }
    }

    return coords;
}

function coordExists(coords, coordToCheck) {
    coords.forEach((coord) => {
        if (coord.x == coordToCheck.x && coord.y == coordToCheck.y) {
            return true;
        }
    });

    return false;
}

function getTotalClicksForPage(pageId){
    let totalClicks = 0;

    generatedSchema.ElementsDailyStats.forEach((elem) => {
        if (elem.pageId == pageId){
            totalClicks += elem.clicks;
        }
    });

    return totalClicks;
}

generateDataBase();
fs.writeFileSync('./db-data.json', JSON.stringify(generatedSchema, null, 4));
logger.info('DB Data written to db-data.json');
