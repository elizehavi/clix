'use strict';

const Client = require('ssh2').Client;
const logger = require('./../lib/logger');
const conn = new Client();
const username = 'webstud';
const host = 'mobedu6.mtacloud.co.il';
const cmd = process.argv[3];

if (!process.argv[2]) {
    logger.error('please provide a password for deploy');
    process.exit(1);
}

if (!process.argv[3]) {
    logger.error('please provide a command for deploy');
    process.exit(1);
}

start();

function start() {
    conn.on('ready', () => {
        logger.info('connected to webmta cloud');

        conn.shell((err, stream) => {
            if (err) {
                throw err;
            }

            stream.setEncoding('utf8');

            stream
                .on('close', () => {
                    logger.info('Connection to webmta cloud has ended');
                    conn.end();
                    process.exit();
                })
                .on('data', (data) => {
                    logger.info(data);
                })
                .stderr.on('data', (data) => {
                    logger.error(data);
                    stream.end();
                });

            stream.end(`${cmd}\nexit\n`);
        });
    });

    conn.connect({
        host: host,
        port: 22,
        username: username,
        password: process.argv[2]
    });
}
