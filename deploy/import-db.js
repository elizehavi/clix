'use strict';

const fs = require('fs');
const dbLayer = require('./../lib/db');
const logger = require('./../lib/logger');
const collections = require('./../lib/config').mongo.collections;
const schema = JSON.parse(fs.readFileSync('./db-data.json'));

let db;

dbLayer.setup()
    .then(() => db = dbLayer.getDb())
    .then(() => db.dropDatabase())
    .then(() => importDb())
    .then(() => createIndexes())
    .then(() => {
        logger.info('import done');
        process.exit();
    })
    .catch((err) => {
        logger.error(err);
        process.exit(1);
    });

function importDb() {
    const tasks = [];
    for (let key of Object.keys(schema)) {
        const collectionName = key;
        const collectionData = schema[key];

        collectionData.forEach((row) => {
            if (row.date) {
                row.date = new Date(row.date);
                row.date.setUTCHours(0, 0, 0, 0);
            }
        });

        logger.info(`processing collection ${collectionName}`);
        tasks.push(db.collection(collectionName).insertMany(collectionData));
    }

    return Promise.all(tasks);
}

function createIndexes() {
    const tasks = [];

    tasks.push(db.collection(collections.Users)
        .createIndex({ email: 1 }));

    tasks.push(db.collection(collections.Pages)
        .createIndex({
            pageId: 1,
            siteId: 1
        }));

    tasks.push(db.collection(collections.PagesDailyStats)
        .createIndex({
            pageId: 1,
            siteId: 1,
            date: 1
        }));

    tasks.push(db.collection(collections.ElementsDailyStats)
        .createIndex({
            elementId: 1,
            pageId: 1,
            siteId: 1,
            date: 1
        }));

    tasks.push(db.collection(collections.HeatMapDailyStats)
        .createIndex({
            pageId: 1,
            siteId: 1,
            date: 1
        }));

    return Promise.all(tasks);
}
