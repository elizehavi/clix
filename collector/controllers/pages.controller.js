'use strict';

const db = require('./../../lib/db');

function save(eventType, siteId, pageId) {
    return db.updatePage(eventType, siteId, pageId);
}

function getAll(siteId) {
    return db.getPages(siteId);
}

module.exports = {
    save: save,
    getAll: getAll
};
