'use strict';

const db = require('./../../lib/db');

function save(hoverBulk) {
    const hoverPromises = hoverBulk.elements.map(
        (hover) => db.updateHover(hoverBulk.siteId, hover));

    return Promise.all(hoverPromises);
}

module.exports = {
    save: save
};
