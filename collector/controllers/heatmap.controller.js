'use strict';

const db = require('./../../lib/db');

function save(heatMapBulk, type) {
    const heatmapEvents = heatMapBulk.elements.map((event) =>
        db.updateHeatMap(event, heatMapBulk.siteId, type));

    return Promise.all(heatmapEvents);
}

module.exports = {
    save: save
};
