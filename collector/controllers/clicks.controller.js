'use strict';

const logger = require('./../../lib/logger');
const db = require('./../../lib/db');

function save(clickBulk) {
    const clickPromises = clickBulk.elements.map(
        (click) => db.updateClick(clickBulk.siteId, click));
    const pageClicks = buildTotalClicksPerPage(clickBulk.elements);
    logger.infoJSON(pageClicks);
    const pagePromises = pageClicks.map(
        (page) => db.updatePageClicks(clickBulk.siteId, page));

    return Promise.all(clickPromises.concat(pagePromises));
}

function buildTotalClicksPerPage(elements) {
    const pages = elements.reduce((pages, elem) => {
        if (!pages[elem.pageId]) {
            pages[elem.pageId] = 0;
        }

        pages[elem.pageId] += elem.clickCount;
        return pages;
    }, {});

    return Object.keys(pages).map((key) => ({ pageId: key, clicks: pages[key] }));
}

module.exports = {
    save: save
};
