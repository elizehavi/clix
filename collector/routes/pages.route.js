'use strict';

const express = require('express');
const logger = require('./../../lib/logger');
const helpers = require('./../../lib/helpers');
const router = express.Router();
const controller = require('./../controllers/pages.controller');

const pageEventTypes = [
    'view', 'visit', 'purchase'
];

router.get('/:type', (req, res) => {

    if (!req.query || !req.query.siteId || !req.query.pageId) {
        return helpers.handleError(res, `pages/${req.params.type} missing required parameters (siteId, pageId)`);
    }

    const eventType = req.params.type;
    const pageId = req.query.pageId;
    const siteId = req.query.siteId;

    if (pageEventTypes.indexOf(eventType) < 0) {
        logger.error(`GET /pages/${eventType} is not a valid route.`);
        return res.status(500).send(`/pages/${eventType} is not a valid route.`);
    }

    controller
        .save(eventType, siteId, pageId)
        .then(() => res.send(`page ${pageId} ${eventType} event saved in bulk.`))
        .catch((err) => {
            helpers.handleError(res, `pages/${req.params.type}`, err);
        });
});

module.exports = router;
