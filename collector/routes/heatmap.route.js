/**
 * Created by Hagai on 10/09/2016.
 */

'use strict';
// url to test = http://localhost:8080/heatmap/clicks
const logger = require('./../../lib/logger');
const helpers = require('./../../lib/helpers.js');
const express = require('express');
const router = express.Router();
const controller = require('./../controllers/heatmap.controller');

router.post('/:type', (req, res) => {
    logger.debug('body is ' + JSON.stringify(req.body, null, 4));

    const eventType = req.params.type;

    if(eventType == 'clicks' || eventType == 'events') {

        controller
            .save(req.body, eventType)
            .then(() => {
                return res.send('hovers were saved');
            })
            .catch((err) => helpers.handleError(res, `heatmap/${eventType}`, err));
    }
    else{
        return res.status(500).send(`${eventType} is an invalid event-type`);
    }
});

module.exports = router;