'use strict';

const logger = require('./../../lib/logger');
const helpers = require('./../../lib/helpers.js');
const express = require('express');
const router = express.Router();
const controller = require('./../controllers/clicks.controller');

router.post('/', (req, res) => {
    logger.debug('body is ' + JSON.stringify(req.body, null, 4));

    controller
        .save(req.body)
        .then(() => {
            return res.send('clicks were saved');
        })
        .catch((err) => helpers.handleError(res, 'clicks', err));
});

module.exports = router;
