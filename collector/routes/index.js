'use strict';

const clicks = require('./clicks.route');
const hovers = require('./hovers.route');
const heatMap = require('./heatmap.route');
const pages = require('./pages.route');

function setup(app) {
    app.use('/clicks', clicks);
    app.use('/hovers', hovers);
    app.use('/heatmap', heatMap);
    app.use('/pages', pages);
}

module.exports = setup;
