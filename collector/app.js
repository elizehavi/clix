'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./../lib/logger');
const db = require('./../lib/db');
const routes = require('./routes');
const app = express();

/*
 * Cross Middlewares
 */
app.use(bodyParser.json());

/*
 * Routes
 */
routes(app);

/*
 * Exception Handling
 */
app.use((err, req, res, next) => {
    logger.error(err.stack);

    return res.status(500).send('Unexpected ERror');
});

/*
 * DB Setup and Initialization
 */
db.setup()
    .then(() => {
        app.listen(8080, function() {
            logger.info('collector is listening on port 8080!');
        });
    })
    .catch((err) => {
        logger.error(`Please verify mongodb is online: ${err}`);
        process.exit(1);
    });
